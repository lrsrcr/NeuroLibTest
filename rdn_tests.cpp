﻿#include <iostream>
#include <fstream>
#include <sstream>

#include <Apprentissage/apprentissage.h>

#include "utils.h"

using namespace std;

// Essais RDN
void EssaiRDN();
void EssaiRDN_XOR();
void EssaiRDN_Moon();
void EssaiRDN_Iris(); // Ne fonctionne pas bien
void EssaiGradientCheck(); // Ne fonctionne pas bien

void EssaiSGD();
void EssaiMinibatch();

// A essayer avec la fonction Softmax implémentée
void EssaiRDN_Moon_CrossEntropy();
void EssaiRDN_Toy();

void EssaiSoftmax(); // Essai d'implémentation Softmax
void EssaiMelangerDonnees(); // Essai de mon algorithme pour mélanger les données
void EssaiMoonSoftmax();
void ExempleSoftmax();

void ApproximerFonction();

void ExerciceRetropropagation();

int main2()
{
  cout << "Hello World!" << endl;

  //Essais RDN:
  //EssaiRDN();
  //EssaiRDN_XOR();

  //EssaiGradientCheck();
  //EssaiRDN_Moon();
  //EssaiRDN_Moon_CrossEntropy();
  //EssaiRDN_Toy(); // Ne fonctionne pas bien

  //EssaiSoftmax();
  //EssaiMelangerDonnees();
  //EssaiSGD();
  //EssaiMinibatch();

  //EssaiMoonSoftmax();
  //ExempleSoftmax();

  //ApproximerFonction();

  //ExerciceRetropropagation();

  return 0;
}

void ExerciceRetropropagation()
{
    ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 2, 1, 1);
    rdn.couches[0][0].poids={2, 4};  rdn.couches[0][0].biais=1.0f;
    rdn.couches[0][1].poids={-3, 8}; rdn.couches[0][1].biais=1.0f;
    rdn.couches[1][0].poids={6, -8}; rdn.couches[1][0].biais=1.0f;

    vector<vector<double>> x = {{-5, 9}},
        y = {{1}};

    RetropropagationGradient(rdn, x, y, 1, 0.1, 0.05, true);

    // Les poids vont dans le bon sens (vers les négatif) mais je n'atteins pas les mêmes résultats que sur le site
    cout << "Poids après backprop: " << endl
         << rdn.PoidsToString() << endl;
}

void ApproximerFonction()
{
    string nomFichier="/home/julien/workspace/NeuroLibTest/approximer_fonction_resultat.csv";
    size_t nbEntrees=1, nbSorties=1, nbcouches = 5;
    vector<vector<double>> x, y, y1;
    LireFichierCSV("/home/julien/workspace/NeuroLibTest/approximer_fonction.csv", x, nbEntrees, y, nbSorties);

    ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, nbcouches, nbcouches, 1);
    rdn.couches[nbcouches][0].fonctionActivation = Lineaire;
    rdn.couches[nbcouches][0].fonctionActivationDerivee = LineaireDerivee;
    RetropropagationGradient(rdn, x, y, 1000, 0.001f, 0.05f, true);
    //DescenteGradientStochastique(rdn, x, y, 10, 0.01);

    EnregistrerPrediction(nomFichier, -5, 5, 0.2, rdn);
}

void ExempleSoftmax()
{ // Exemple de : https://gormanalysis.com/neural-networks-a-worked-example/
  vector<vector<double>> x = { {252, 4, 155, 175}, {175, 10, 186, 200}, {82, 131, 230, 100}, {115, 138, 80, 88}},
      y = {{1,0}, {1,0}, {0,1}, {0,1}};
  double nbEntrees = 4;
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 2, 1, 2);
  rdn.couches[0][0].biais = -0.00469; rdn.couches[0][0].poids[0] = -0.00256; rdn.couches[0][0].poids[1] = 0.00146;  rdn.couches[0][0].poids[2] = 0.00816;  rdn.couches[0][0].poids[3] = -0.00597;
  rdn.couches[0][1].biais = 0.00797; rdn.couches[0][1].poids[0] = 0.00889; rdn.couches[0][1].poids[1] = 0.00322;  rdn.couches[0][1].poids[2] = 0.00258;  rdn.couches[0][1].poids[3] = -0.00876;
  rdn.couches[1][0].biais = -0.00588; rdn.couches[1][0].poids[0] = -0.00647; rdn.couches[1][0].poids[1] = 0.00374;
  rdn.couches[1][1].biais = -0.00232; rdn.couches[1][1].poids[0] = 0.00540; rdn.couches[1][1].poids[1] = -0.00005;
  rdn.isSoftmax=true;

  /*double errTmp = 0.0f;
  for(size_t i=0; i<1; i++)
  {
    vector<double> &sorties = rdn.Evaluer(x[i]);

    cout << "Exemple " << i << ": y estimes: ";
    for(double s : sorties)
       cout << s << '\t';
    errTmp += FonctionPerteEntropieRelativeGenerale(sorties, y[i]);
    cout << FonctionPerteEntropieRelativeGenerale(sorties, y[i]) << endl;
  }

  cout << "CE = " << errTmp/1 << endl
       << "Retropropagation couche sortie" << endl;

  rdn.Evaluer(x[0]);
  double erreurGlobale = CalculerErreurGlobale(rdn.getCoucheSortie(), y[0], false, true);
  cout << "CE globale " << erreurGlobale << endl;*/


/*
  vector<double> deriveeSortie(2);
  CorrectionPerteEntropieRelativeGenerale(rdn.sortiesReseau[rdn.getIndiceCoucheSortie()], y[0], deriveeSortie);

  cout << "Pour exemple 0: ";
  for(double dS : deriveeSortie)
    cout << dS << '\t';
  cout << endl;*/
}

void EssaiMoonSoftmax()
{
  //Problème: la vérification du gradient à un facteur x2 avec la correction à Softmax!!!
  cout << "EssaiRDN Moon Softmax" << endl;
  size_t nbEntrees=2;
  vector<vector<double>> x, y, y1;
  //double seuil=0.01;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);

  for(double yi : y[0])
  {
    vector<double> tmp;

    if(yi==1) // Classe 1
    {
      tmp.push_back(1);
      tmp.push_back(0);
    }
    else if(yi==0) // Classe 0
    {
      tmp.push_back(0);
      tmp.push_back(1);
    }

    y1.push_back(tmp);
  }

  // 2 Classes avec Softmax, donc 2 neurones de sorties
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 5, 1, 3);
  rdn.isSoftmax=true;
  RetropropagationGradient(rdn, x, y, 1, 0.01f, 0.05f, false);
  //DescenteGradientStochastique(rdn, x, y1, 5000, 0.001, seuil, false);
  //DescenteGradientStochastiqueMB(rdn, x, y1, 5000, 40, 0.1, seuil, true);
  //DescenteGradientStochastiqueMB(rdn, x, y1, 5000, 40, 0.09, seuil, false);

  /*double xmin = -3, xmax=3,
      ymin=-3, ymax=3,
      pas=0.1;
  EnregistrerLimiteDecision("moon_BL_Softmax.csv", xmin, xmax, ymin, ymax, pas, rdn);*/
}

void EssaiMelangerDonnees()
{
  vector<size_t> indexes;
  size_t nbDonnees=10;

  for(size_t i=0; i<5; i++)
  {
    MelangerIndexes(nbDonnees, indexes);

    cout << "Iteration " << i << ": ";
    for(size_t j=0; j<nbDonnees; j++)
      cout << indexes[j] << ' ';
    cout << endl;
  }
}

void EssaiSoftmax()
{
  // Essai d'implémentation Softmax
  /*vector<double> scores = {3.0f, 1.0f, 0.2f};
  double denominateur = 0.0f;
  for(double d : scores)
    denominateur += exp(d);

  vector<double> probabilites(scores.size());
  double somme;
  cout << '[' << '\t';
  for(size_t i=0; i<probabilites.size(); i++)
  {
    probabilites[i] = exp(scores[i])/denominateur;
    somme += probabilites[i];
    cout << probabilites[i] << '\t';
  }

  cout << ']' << endl << "Somme: " << somme << endl;*/

  // Essai implémentation cross-entropy + softmax
  /*double[] coucheSortie={0.2698, 0.32235, 0.40784},
      one_hot_encoded = {1.0f, 0.0f, 0.0f};*/

}

void EssaiRDN_Toy()
{
  // Essayer de faire comme les exemples de http://neuralnetworksanddeeplearning.com/chap3.html#the_cross-entropy_cost_function
  ReseauNeurone rdn(1, Logistique, LogistiqueDerivee, 0, 0, 1);
  rdn.couches[0][0].poids={0.6f}; rdn.couches[0][0].biais=0.9f;

  vector<vector<double>> x = {{1}},
      y = {{0}};

  RetropropagationGradient(rdn, x, y, 300, 0.005, 0.05, true);

  // Les poids vont dans le bon sens (vers les négatif) mais je n'atteins pas les mêmes résultats que sur le site
  cout << "Poids après backprop: " << endl
       << rdn.PoidsToString() << endl;
}

void EssaiRDN_Moon_CrossEntropy()
{
  cout << "EssaiRDN_Moon_CrossEntropy" << endl;
  size_t nbEntrees=2;
  vector<vector<double>> x, y;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);

  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 9, 1, 1);
  DescenteGradientStochastique(rdn, x, y, 5000, 0.01, 0.01, false);


  cout << rdn.SortiesToString() << endl;

  cout << "essai: " << endl;
  size_t nbErreurs=0;
  double seuil=0.1;
  for(size_t i=0; i<x.size(); i++)
  {
    vector<double> exemple=x[i];
    rdn.Evaluer(exemple);
    double sortie=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0],
	output=0.0f;
    cout << i << " Résultat: " << rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0] << endl;

    if(sortie>0.5)
      output=1.0f;
    else
        output=0.0f;

    if(output!=y[i][0])
      nbErreurs++;
  }
  cout << "Nombre d'erreurs (seuil =" << seuil << "): " << nbErreurs << endl;

  //Pour pouvoir tracer les points dans excel/calc
  double xmin = -3, xmax=3,
      ymin=-3, ymax=3,
      pas=0.1;

  EnregistrerLimiteDecision("moon_BL_CE.csv", xmin, xmax, ymin, ymax, pas, rdn);
}

void EssaiGradientCheck()
{
  vector<vector<double>> deltasB, deriveesBiais;
  vector<vector<vector<double>>> deltasWij, deriveesPoids;
  vector<vector<double>> x,y,y1;
  size_t nbEntrees=2;
  /*vector<vector<double>> x={{0,0} , {1,0}, {0,1}, {1,1 } };
  vector<vector<double>> y={ {0} , {1}, {1}, {0} };*/

/*  cout << "Essai XOR " << endl;
  ReseauNeurone rdn(x[0].size(), Logistique, LogistiqueDerivee, 1, 1, 1);*/

  /*cout << "Essai XOR 2" << endl;
  ReseauNeurone rdn(2, Logistique, 2, 1, 1);

  // Définir biais
  rdn.couches[0][0].biais=-0.4635107399577998; rdn.couches[0][1].biais=0.09750161997450091;
  rdn.couches[1][0].biais=0.7792991203673414;

  // Définir poids
  // {{-5.98, -6.12}, {-1.11, -1.14}}, {{19.35, -59.51}}
  rdn.couches[0][0].poids={-0.06782947598673161, 0.22341077197888182}; rdn.couches[0][1].poids={0.9487814395569221, 0.461587116462548};
  rdn.couches[1][0].poids={-0.22791948943117624, 0.581714099641357};*/

  /*cout << "Essai avec le RDN pour Moon" << endl;
  x.clear(); y.clear();
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);
  for(size_t j=0; j<y.size(); j++)
  {
    for(double yi : y[j])
    {
      vector<double> tmp;

      if(yi==1) // Classe 1
      {
	tmp.push_back(1);
	tmp.push_back(0);
      }
      else if(yi==0) // Classe 0
      {
	tmp.push_back(0);
	tmp.push_back(1);
      }

      y1.push_back(tmp);
    }
  }
  //ReseauNeurone rdn(2, TangenteHyperbolique, TangenteHyperboliqueDerivee, 3, 5, 1); // moon.csv
  ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 3, 5, 2); // moon_class0.csv
  rdn.isSoftmax=true;
  VerifierGradient(rdn, x, y1, deltasB, deltasWij, deriveesBiais, deriveesPoids, 1e-7, false);*/

  /*cout << "Essai avec Softmax" << endl;
  size_t nbEntrees=2;
  vector<vector<double>> x, y, y1;
  //double seuil=0.05;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);

  for(double yi : y[0])
  {
    vector<double> tmp;

    if(yi==1) // Classe 1
    {
      tmp.push_back(1);
      tmp.push_back(0);
    }
    else if(yi==0) // Classe 0
    {
      tmp.push_back(0);
      tmp.push_back(1);
    }
  }

  // 2 Classes avec Softmax, donc 2 neurones de sorties
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 5, 1, 2);
  rdn.isSoftmax=true;
  VerifierGradient(rdn, x, y1, deltasB, deltasWij, deriveesBiais, deriveesPoids, 1e-7, false);*/

  cout << "Essai avec Iris" << endl;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-3classes.csv", x, nbEntrees, y, 3);
  ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 3, 5, 3);
  rdn.isSoftmax=true;
  VerifierGradient(rdn, x, y, deltasB, deltasWij, deriveesBiais, deriveesPoids, 1e-7, false);
}

void EssaiMinibatch()
{
  vector<vector<double>> x, y, y1;
  double seuil=0.01, tauxApprentissage=0.1;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, 2, y, 1);

  for(double yi : y[0])
  {
    vector<double> tmp;
    tmp.push_back(yi);
    y1.push_back(tmp);
  }

  //ReseauNeurone rdn(2, TangenteHyperbolique, TangenteHyperboliqueDerivee, 5, 1, 1);
  ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 5, 1, 1); //Résultats très mauvais...
  //seuil=0.1; tauxApprentissage=0.1;
  DescenteGradientStochastiqueMB(rdn, x, y1, 5000, x.size()/10, tauxApprentissage, seuil);

  /*cout << "essai: " << endl;
  size_t nbErreurs=0;
  for(size_t i=0; i<x.size(); i++)
  {
    vector<double> exemple=x[i];
    rdn.Evaluer(exemple);
    double sortie=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0];
    cout << i << " Résultat: " << rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0] << endl;

    double erreur=abs(y1[i][0]-sortie);

    if(erreur>0.1)
      nbErreurs++;
  }
  cout << "Nombre d'erreurs (seuil =" << seuil << "): " << nbErreurs << endl;

  //Pour pouvoir tracer les points dans excel/calc
  double xmin = -3, xmax=3,
      ymin=-3, ymax=3,
      pas=0.1;

  EnregistrerLimiteDecision("moon_BL_SGDMB.csv", xmin, xmax, ymin, ymax, pas, rdn);*/
}

void EssaiSGD()
{
  size_t nbEntrees=2;
  vector<vector<double>> x, y, y1;
  double seuil=0.01, tauxApprentissage=0.1;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);

  for(double yi : y[0])
  {
    vector<double> tmp;
    tmp.push_back(yi);
    y1.push_back(tmp);
  }

  //ReseauNeurone rdn(nbEntrees, TangenteHyperbolique, TangenteHyperboliqueDerivee, 5, 1, 1);
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 5, 1, 1);
  DescenteGradientStochastique(rdn, x, y1, 5000, tauxApprentissage, seuil);

  cout << "essai: " << endl;
  size_t nbErreurs=0;
  for(size_t i=0; i<x.size(); i++)
  {
    vector<double> exemple=x[i];
    rdn.Evaluer(exemple);
    double sortie=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0];
    cout << i << " Résultat: " << rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0] << endl;

    double erreur=abs(y1[i][0]-sortie);

    if(erreur>0.1)
      nbErreurs++;
  }
  cout << "Nombre d'erreurs (seuil =" << seuil << "): " << nbErreurs << endl;

  //Pour pouvoir tracer les points dans excel/calc
  double xmin = -3, xmax=3,
      ymin=-3, ymax=3,
      pas=0.1;

  EnregistrerLimiteDecision("moon_BL_SGD.csv", xmin, xmax, ymin, ymax, pas, rdn);
}

void EssaiRDN_Iris()
{
  size_t nbEntrees=4;
  vector<vector<double>> x, x1, y, y1;
  double seuil=0.05;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-nn.csv", x, nbEntrees, y, 3);
  x1.push_back(x[0]);
  y1.push_back(y[0]);
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 2, 1, 3);
  cout << "Poids avant backprop: " << rdn.PoidsToString() << endl;
  RetropropagationGradient(rdn, x1, y, 1, 0.25, seuil);
}

void EssaiRDN_Moon()
{
  size_t nbEntrees=2;
  vector<vector<double>> x, y, y1;
  double seuil=0.01, seuil2=0.5;

  /*LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon.csv", x, nbEntrees, y, 1);
  for(double yi : y[0])
  {
    vector<double> tmp;
    tmp.push_back(yi);
    y1.push_back(tmp);
  }

  ReseauNeurone rdn(nbEntrees, TangenteHyperbolique, TangenteHyperboliqueDerivee, 5, 1, 1);
  RetropropagationGradient(rdn, x, y1, 5000, 0.01, seuil);*/

  LireFichierCSV("/home/julien/workspace/NeuroLibTest/moon_class0.csv", x, nbEntrees, y, 1);
  ReseauNeurone rdn(nbEntrees, Logistique, LogistiqueDerivee, 5, 2, 1);

  RetropropagationGradient(rdn, x, y, 5000, 0.1, seuil);
  cout << rdn.SortiesToString() << endl;

  cout << "essai: " << endl;
  size_t nbErreurs=0;
  for(size_t i=0; i<x.size(); i++)
  {
    vector<double> exemple=x[i];
    rdn.Evaluer(exemple);
    double sortie=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0],
	output=0.0f;
    cout << i << " Résultat: " << rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0] << endl;

    if(sortie>seuil2)
      output=1.0f;

    if(output!=y1[i][0])
      nbErreurs++;
  }
  cout << "Nombre d'erreurs (seuil =" << seuil << "): " << nbErreurs << endl;

  //Pour pouvoir tracer les points dans excel/calc
  double xmin = -3, xmax=3,
      ymin=-3, ymax=3,
      pas=0.1;

  EnregistrerLimiteDecision("moon_BL.csv", xmin, xmax, ymin, ymax, pas, rdn);
}

void EssaiRDN_XOR()
{
  // http://www.heatonresearch.com/aifh/vol3/multi_layer.html
  cout << "Essai XOR " << endl;
  vector<vector<double>> x={{0,0}, {1,0}, {0,1}, {1,1 }};
  vector<vector<double>> y={ {0}, {1}, {1}, {0} };
  ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 2, 1, 1);

  // Définir biais
  rdn.couches[0][0].biais=-0.4635107399577998; rdn.couches[0][1].biais=0.09750161997450091;
  rdn.couches[1][0].biais=0.7792991203673414;

  // Définir poids
  // {{-5.98, -6.12}, {-1.11, -1.14}}, {{19.35, -59.51}}
  rdn.couches[0][0].poids={-0.06782947598673161, 0.22341077197888182}; rdn.couches[0][1].poids={0.9487814395569221, 0.461587116462548};
  rdn.couches[1][0].poids={-0.22791948943117624, 0.581714099641357};

  RetropropagationGradient(rdn, x, y, 10000, 0.7f, 0.01);
  //RetropropagationGradient(rdn, x, y, 1, 0.7f, 0.0001);

  cout << "Poids après backprop: " << endl << rdn.PoidsToString() << endl;
  cout << "essai après backprop: " << endl;
  double erreurTmp = 0.0f;
  size_t i=0;
  for(vector<double> exemple : x)
  {
    rdn.Evaluer(exemple);
    cout << "Résultat: " << rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0] << endl;
    erreurTmp+= pow(rdn.couches[1][0].y -  y[i++][0],2);
  }

  erreurTmp/=4;
  cout << "MSE: " << erreurTmp << endl;

  double xmin = -1.5, xmax=1.5,
      ymin=-1.5, ymax=1.5,
      pas=0.1;
  EnregistrerLimiteDecision("xor_rdn.csv", xmin, xmax, ymin, ymax, pas, rdn);
}

void EssaiRDN()
{
  // http://www.heatonresearch.com/aifh/vol3/multi_layer.html
  vector<vector<double>> y={ {0}, {1}, {1}, {0} };
  vector<double> x={0, 1};

  ReseauNeurone rdn(2, Logistique, LogistiqueDerivee, 2, 1, 1);
  // Définir biais
  rdn.couches[0][0].biais=8.35; rdn.couches[0][1].biais=-0.52;
  rdn.couches[1][0].biais=-2.27;

  // Définir poids
  // {{-5.98, -6.12}, {-1.11, -1.14}}, {{19.35, -59.51}}
  rdn.couches[0][0].poids={-5.98, -6.12}; rdn.couches[0][1].poids={-1.11, -1.14};
  rdn.couches[1][0].poids={19.35, -59.51};

  rdn.Evaluer(x);
  cout << "Sorties du RDN:" << endl << rdn.SortiesToString() << endl;
}
