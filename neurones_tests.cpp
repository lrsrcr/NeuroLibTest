﻿#include <Apprentissage/apprentissage.h>

#include "utils.h"

using namespace std;

// Essais perceptron/neurone
void EssaiSimple();
void EssaiXOR();
void EssaiOR();
void EssaiObesite();
void EssaiIris1();
void EssaiIris2();
void SeparationSimple();

int main()
{
  cout << "Hello world" << endl;

  //Essai perceptron / neurone:
  //EssaiSimple();
  //EssaiOR();
  //EssaiXOR();

  //EssaiObesite();
  //EssaiIris1();
  EssaiIris2();
  //SeparationSimple();

  return 0;
}

void SeparationSimple()
{
  vector<vector<double>> x;
  vector<double> y;
  size_t nbEntrees=2;
  double (*ptrfa)(double)=Signe;//pointeur fonction activation

  Neurone n(nbEntrees, ptrfa);
  //LireFichierCSV("/home/julien/workspace/NeuroLibTest/separation_simple2_adaline.csv", x, nbEntrees, y);
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/separation_simple_adaline.csv", x, nbEntrees, y);

  //DescenteGradientWidrowHoff(n, x, y, 21, 0.01, 0.01);
  //ReglePerceptron(n, x, y, 50, 0.1);
  DescenteGradient(n, x, y, 50, 2E-3);

  cout << "Poids après apprentissage: " << endl
       << n.PoidsToString() << endl;
}

/*
 * NE PAS ESSAYER TOUS LES APPRENTISSAGES A LA FOIS
 */
void EssaiIris2()
{
  size_t nbEntrees=2;
  cout << "Essai Iris à 3 classes" << endl;
  double (*ptrfa)(double)=Signe;//pointeur fonction activation
  vector<double> w(0);
  vector<vector<double>> x(0), y(0);

  Neurone n1(nbEntrees, ptrfa), n2(nbEntrees, ptrfa), n3(nbEntrees, ptrfa);

  /*LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-3classes.csv", x, nbEntrees, y, 3);
  cout << "Utilisation de la fct Regle Perceptron" << endl;
  ReglePerceptron(n1, x, y[0], 200, 0.01);
  ReglePerceptron(n2, x, y[1], 200, 0.01);
  ReglePerceptron(n3, x, y[2], 200, 0.01);*/

  /*cout << "Utilisation de la fct ConvergencePerceptron" << endl;
  ConvergencePerceptron(n1, x, y[0], 100);
  ConvergencePerceptron(n2, x, y[1], 100);
  ConvergencePerceptron(n3, x, y[2], 100);*/

  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-3classes-adaline.csv", x, nbEntrees, y, 3);
  ptrfa=Signe; //Descente de gradient et Adaline requièrent tous deux des FA linéaires!
  n1=Neurone(nbEntrees,ptrfa); n2=Neurone(nbEntrees,ptrfa);  n3=Neurone(nbEntrees,ptrfa);

  // transposer matrice y (150x3) en (3 (=neurones) x 150 (=exemples) )! :
  vector<vector<double>> y1(3);
  for(size_t i=0; i<y1.size(); i++)
    y1[i]=vector<double>(y.size());

  for(size_t i=0; i<y.size(); i++)
    for(size_t j=0; j<3; j++)
      y1[j][i] = y[i][j];

  cout << "Utilisation de descente du gradient ADALINE" << endl;
  DescenteGradientWidrowHoff(n1, x, y1[0], 200, 0.01, 0.0001);
  DescenteGradientWidrowHoff(n2, x, y1[1], 3000, 0.01, 0.0001);
  DescenteGradientWidrowHoff(n3, x, y1[2], 200, 0.01, 0.0001);

  /*cout << "Utilisation de descente du gradient CLASSIQUE" << endl;
  DescenteGradient(n1, x, y1[0], 200, 0.0001);
  DescenteGradient(n2, x, y1[1], 200, 0.0001);
  DescenteGradient(n3, x, y1[2], 200, 0.0001);*/

  size_t erreur1=Evaluation(&n1, &x, &y1[0]);
  size_t erreur2=Evaluation(&n2, &x, &y1[1]);
  size_t erreur3=Evaluation(&n3, &x, &y1[2]);

  cout << "Nombre d'erreur pour les données: "
      << "\nErreur 1 : " << erreur1 << endl
      << "\nErreur 2 : " << erreur2 << endl
      << "\nErreur 3 : " << erreur3 << endl;

  cout << "Poids neurone 1: " << n1.PoidsToString() << endl
       << "Poids neurone 2: " << n2.PoidsToString() << endl
       << "Poids neurone 3: " << n3.PoidsToString() << endl;
}

void EssaiIris1()
{
  size_t nbEntrees=2;
  //0 = iris setosa; 1= Iris versicolor
  cout << "Essai Iris à 2 classes" << endl;
  double (*ptrfa)(double)=Heaviside;//pointeur fonction activation
  vector<double> y(0), yTest(0);
  vector<vector<double>> x(0), xTest(0);

  Neurone n(nbEntrees, ptrfa), n1(nbEntrees, ptrfa);

  /*LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris.csv", x, nbEntrees, y);
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-test.csv", xTest, nbEntrees, yTest);*/

  /*cout << "Essai avec correction" << endl;
  //ConvergencePerceptron(n, x, y, 100); // Converge pas du tout
  ReglePerceptron(n, x, y, 100);
  Evaluation(&n, &xTest, &yTest);

  cout << "Poids de sortie: " << n.PoidsToString() << endl;*/

  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris_adaline.csv", x, nbEntrees, y);
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/iris-test_adaline.csv", xTest, nbEntrees, yTest);

  ptrfa=Signe; //Descente de gradient et Adaline requièrent tous deux des FA linéaires!*/

  cout << "Utilisation de descente du gradient CLASSIQUE" << endl;
  /*n1=Neurone(nbEntrees, ptrfa);
  DescenteGradient(n1, x, y, 1000, 0.0001);
  Evaluation(&n1, &xTest, &yTest);*/

  cout << "Utilisation de descente du gradient ADALINE" << endl;
  n=Neurone(nbEntrees, ptrfa);
  DescenteGradientWidrowHoff(n, x, y, 100, 0.0001);
  Evaluation(&n, &xTest, &yTest);
}

void EssaiObesite()
{
  int nbEntree=2;
  cout << "Essai Obesite" << endl;
  double (*ptrfa)(double)=Heaviside;//pointeur fonction activation
  vector<double> y(0), yTest(0);
  vector<vector<double>> x(0), xTest(0);

  Neurone n(nbEntree, ptrfa), n1(nbEntree, ptrfa);
  cout << "Essai avec correction" << endl;
/*
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/obesite.csv", x, nbEntree, y);
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/obesitetest.csv", xTest, nbEntree, yTest);
  ConvergencePerceptron(n, x, y, 50);
  Evaluation(&n1, &xTest, &yTest);

  ReglePerceptron(n, x, y);
  Evaluation(&n1, &xTest, &yTest);
*/
  n.fonctionActivation=Signe;
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/obesite_adaline.csv", x, nbEntree, y);
  LireFichierCSV("/home/julien/workspace/NeuroLibTest/obesitetest_adaline.csv", xTest, nbEntree, yTest);
  //DescenteGradient(n, x, y, 100, 0.000000000001); // Ne converge jamais mais de donne pas de mauvais résultats à l'évaluation
  DescenteGradientWidrowHoff(n, x, y, 100); //Converge

  //Evaluation(&n, &xTest, &yTest);
}

void EssaiSimple()
{
  double (*ptrfa)(double)=Logistique;//pointeur fonction activation
  vector<double> w(0), x(0);
  w.push_back(0.1); w.push_back(0.2); /* w.push_back(50);*/
  x.push_back(0.3); x.push_back(0.3); /* x.push_back(3);*/
  Neurone n(w, *ptrfa);
  n.biais=0.3;

  n.Evaluer(x);
  cout << "Potentiel du neurone: " << n.potentiel
       << "Neurone sortie = " << n.y << endl;
}

void EssaiXOR()
{
  cout << "Essai XOR" << endl;
  double (*ptrfa)(double)=Heaviside;//pointeur fonction activation
  vector<double> y(0);
  vector<vector<double>> x(0);
  x.push_back({0.0, 0.0});   x.push_back({0.0, 1.0});
  x.push_back({1.0, 0.0});   x.push_back({1.0, 1.0});

  y.push_back(0.0); y.push_back(1.0);
  y.push_back(1.0); y.push_back(0.0);

  Neurone n(2, ptrfa), n1(2, ptrfa);

  /*ConvergencePerceptron(n, x, y, 50);
  Evaluation(&n1, &x, &y);*/

  /*ReglePerceptron(n1, x, y, 15);
  Evaluation(&n1, &x, &y);*/

  ptrfa=Signe; // pointeur fonction activation
  y[0]=-1; y[3]=-1;
  x[0]={-1.0, -1.0}; x[1]={-1.0, 1.0};
  x[2]={1.0, -1.0};

  /*n=Neurone(2, ptrfa);
  DescenteGradient(n, x, y, 50, 0.1);
  Evaluation(&n, &x, &y, 0.0f);*/

  n=Neurone(2, ptrfa);
  DescenteGradientWidrowHoff(n, x, y, 50);
  Evaluation(&n, &x, &y);
}

void EssaiOR()
{
  cout << "Essai OR" << endl;
  double (*ptrfa)(double)=Heaviside; // pointeur fonction activation
  vector<double> y(0);
  vector<vector<double>> x(0);
  x.push_back({0.0, 0.0});   x.push_back({0.0, 1.0});
  x.push_back({1.0, 0.0});   x.push_back({1.0, 1.0});

  y.push_back(0.0); y.push_back(1.0);
  y.push_back(1.0); y.push_back(1.0);

  /*Neurone n(2, ptrfa);
  //ConvergencePerceptron(n, x, y, 50); // Ne converge pas à cause du biais
  ReglePerceptron(n, x, y, 50, 1.0); // Converge mais donne un biais = -1.0 ...*/

  ptrfa=Signe; // ptr fonction activation
  x[0] = {-1.0f, -1.0f}; x[1] = {-1.0f, 1.0f}; x[2] = {1.0f, -1.0f};
  y[0]=-1.0;

  Neurone n(2, ptrfa);
  cout << "Essai Descente gradient" << endl;
  DescenteGradient(n, x, y, 50, 0.1);


  /*Neurone n(2, ptrfa);
  cout << "Essai Descente gradient Widrow Hoff" << endl;
  DescenteGradientWidrowHoff(n, x, y, 50, 0.05);*/

  Evaluation(&n, &x, &y);
}
