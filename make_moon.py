#!/usr/bin/python3
# Package imports 
import matplotlib.pyplot as plt 
import numpy as np 
import sklearn 
import sklearn.datasets 
import sklearn.linear_model 
import matplotlib
import csv

# Generate a dataset and plot it
np.random.seed(0)
X, y = sklearn.datasets.make_moons(200, noise=0.20)
#plt.scatter(X[:,0], X[:,1], s=40, c=y, cmap=plt.cm.Spectral)
#plt.show()

x1=[[]]

for c1,c2,y1 in zip(X,y):
	x1.append([c1,c2,y1])	

with open('moon.csv', 'w', newline='') as csvfile:
	writer = csv.writer(csvfile, delimiter=';',
                            quotechar='\'', quoting=csv.QUOTE_MINIMAL)
#	writer.writerow(X)
#	writer.writerow(y)	
	writer.writerow(x1)
