﻿#include "utils.h"

void EnregistrerPrediction(const string &nomFichier, double xmin, double xmax, double pas, ReseauNeurone &rdn)
{
  cout << "Ecriture dans le fichier: " << nomFichier << endl;
  size_t quantite_x = (size_t) (xmax-xmin)/pas;

  vector<double> y(quantite_x);

  ostringstream strs;

  double xbase=xmin;
  for(size_t i=0; i<quantite_x; i++)
  {
      vector<double> x={xbase};
      rdn.Evaluer(x);
      y[i]=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0];

      strs << xbase << ',' << y[i] << endl;

      xbase+=pas;
  }

  std::ofstream outfile (nomFichier);
  outfile << strs.str();
  outfile.close();
}

void EnregistrerLimiteDecision(const string &nomFichier, double xmin, double xmax, double ymin, double ymax, double pas, ReseauNeurone &rdn)
{
  size_t quantite_x = (size_t) (xmax-xmin)/pas,
      quantite_y = (size_t) (ymax-ymin)/pas;

  double** z = new double*[quantite_y];
  for(size_t i = 0; i < quantite_y; ++i)
      z[i] = new double[quantite_x];

  ostringstream strs;

  double ybase=ymin;
  for(size_t i=0; i<quantite_y; i++)
  {
    double xbase=xmin;
    for(size_t j=0; j<quantite_x; j++)
    {
      vector<double> x={xbase,ybase};
      rdn.Evaluer(x);
      z[i][j]=rdn.sortiesReseau[rdn.sortiesReseau.size()-1][0];

      strs << xbase << ',' << ybase << ',' << z[i][j] << endl;

      xbase+=pas;
    }

    ybase+=pas;
  }

  std::ofstream outfile (nomFichier);
  outfile << strs.str();
  outfile.close();


  for(size_t i = 0; i < quantite_y; ++i)
      delete [] z[i];
  delete [] z;
}

size_t Evaluation(Neurone *n,vector<vector<double>> *x, vector<double> *y)
{
  unsigned int nbErreurs=0;
  for(size_t i=0; i<x->size(); i++)
  {
   double sortie=n->Evaluer(x->at(i));

   //cout << "Sortie obtenue: " << sortie << " Sortie attendue: " << y->at(i) << endl;

   if(sortie!=y->at(i))
     nbErreurs++;
  }
  cout << "Nombre d'erreur(s): " << nbErreurs <<  " sur " << x->size() << " valeurs" << endl;

  return nbErreurs;
}

size_t Evaluation(Neurone *n, vector<vector<double>> *x, vector<vector<double>> *y, size_t index)
{
  size_t nbErreurs=0;
  for(size_t i=0; i<x->size(); i++)
  {
   double sortie=n->Evaluer(x->at(i));

   //cout << "Sortie obtenue: " << sortie << " Sortie attendue: " << y->at(i)[index] << endl;

   if(sortie!=y->at(i)[index])
     nbErreurs++;
  }
  cout << "Nombre d'erreur(s): " << nbErreurs <<  " sur " << x->size() << " valeurs" << endl;

  return nbErreurs;
}

void LireFichierCSV(string nomfichier, vector<vector<double>> &x, size_t dimensionsX, vector<double> &y)
{
  char delimiter=',';
  ifstream fichier(nomfichier);
  string ligne;
  size_t i=0;
  while (fichier && getline(fichier, ligne)){
    vector<string> parties=split(ligne, delimiter);
    vector<double> xtmp(dimensionsX);

    for(size_t j=0; j<dimensionsX; j++) //Lire les 2 entrées
    {
      double valeur=stod(parties.at(j));
      xtmp[j]=valeur;
    }

    x.push_back(xtmp);

    //Lire la sortie attendue
    double valeur(stod(parties.back()));
    y.push_back(valeur);
    i++;
  }

  fichier.close();
}

void LireFichierCSV(string nomfichier, vector<vector<double>> &x, size_t dimensionsX, vector<vector<double>> &y, size_t dimensionsY)
{
  char delimiter=',';
  ifstream fichier(nomfichier);
  string ligne;
  size_t i=0;

  /*if(y.size()<dimensionsY)
    y=vector<vector<double>>(dimensionsY);*/

  while (fichier && getline(fichier, ligne))
  {
    vector<string> parties=split(ligne, delimiter);
    vector<double> xtmp(0), ytmp;
    // Lire les entrées attendues
    for(size_t j=0; j<dimensionsX; j++) //Lire les 2 entrées
    {
      double valeur=stod(parties.at(j));
      xtmp.push_back(valeur);
    }
    x.push_back(xtmp);

    // Lire les sorties attendues
    for(size_t j=dimensionsX, k=0; k<dimensionsY; j++, k++) //Lire les différentes entrées
    {
      double valeur=stod(parties[j]);
      ytmp.push_back(valeur);
    }
    y.push_back(ytmp);

    /*for(size_t j=dimensionsX, k=0; k<dimensionsY; j++, k++) //Lire les différentes sorties
    {
      double valeur=stod(parties[j]);
      y[k].push_back(valeur);
    }*/

    i++;
  }

  fichier.close();
}

vector<string> split(const string& s, char delimiter)
{
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter))
  {
    tokens.push_back(token);
  }
  return tokens;
}
