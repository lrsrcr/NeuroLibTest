﻿#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <fstream>
#include <sstream>

#include <Neurone/reseauneurone.h>

using namespace std;

// Fonctions utilitaires
vector<string> split(const string& s, char delimiter);
void LireFichierCSV(string fichier, vector<vector<double>> &x, size_t dimensionsX, vector<double> &y);
void LireFichierCSV(string fichier, vector<vector<double>> &x, size_t dimensionsX, vector<vector<double>> &y, size_t dimensionsY);

void EnregistrerLimiteDecision(const string &nomFichier, double xmin, double xmax, double ymin, double ymax, double pas, ReseauNeurone &rdb);
void EnregistrerPrediction(const string &nomFichier, double xmin, double xmax, double pas, ReseauNeurone &rdn);

size_t Evaluation(Neurone *n, vector<vector<double> > *x, vector<double> *y);
size_t Evaluation(Neurone *n, vector<vector<double> > *x, vector<vector<double> > *y, size_t index);

#endif // UTILS_H
